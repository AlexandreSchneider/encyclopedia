import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'documents',
        children: [
          {
            path: '',
            loadChildren: '../documents/documents.module#DocumentsPageModule'
          }
        ]
      },
      {
        path: 'conseils',
        children: [
          {
            path: '',
            loadChildren: '../conseils/conseils.module#ConseilsPageModule'
          }
        ]
      },
      {
        path: 'rappels',
        children: [
          {
            path: '',
            loadChildren: '../rappels/rappels.module#RappelsPageModule'
          }
        ]
      },
      {
        path: 'partager',
        children: [
          {
            path: '',
            loadChildren: '../partager/partager.module#PartagerPageModule'
          }
        ]
      },
      {
        path: 'accueil',
        children: [
          {
            path: '',
            loadChildren: '../accueil/accueil.module#AccueilPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/accueil',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/accueil',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
