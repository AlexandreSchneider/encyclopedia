import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'accueil', loadChildren: './accueil/accueil.module#AccueilPageModule' },
  { path: 'conseils', loadChildren: './conseils/conseils.module#ConseilsPageModule' },
  { path: 'rappels', loadChildren: './rappels/rappels.module#RappelsPageModule' },
  { path: 'partager', loadChildren: './partager/partager.module#PartagerPageModule' },
  { path: 'documents', loadChildren: './documents/documents.module#DocumentsPageModule' }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
