import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartagerPage } from './partager.page';

describe('PartagerPage', () => {
  let component: PartagerPage;
  let fixture: ComponentFixture<PartagerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartagerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartagerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
